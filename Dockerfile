from archlinux:latest

RUN pacman -Sy kubectl helm helmfile doctl git --noconfirm
RUN helm plugin install https://github.com/databus23/helm-diff
RUN helm plugin install https://github.com/aslafy-z/helm-git --version 0.10.0