Amelia's "Private" Infra
-------------------------

Private as in this is what I run for my own nextcloud and whatever else I feel 
like. It's not exaclty intended to be take whole-cloth, though you are more 
than welcome to do so.

My hope is that I can provide an example of a simple and practical approach to
de-googling your life somewhat.


# Getting Started

## Prerequisites

### Kubectl, Helm, Helmfile

Ideally you can do this via your system package manager. If not refer to each 
of them for installation instructions for your operating system.

### Helm Diff

You will need to install `helm-diff` plugin which can be found here:

https://github.com/databus23/helm-diff

and installed with:

```
helm plugin install https://github.com/databus23/helm-diff
```

### Helm Git

You will need to install `helm-git` plugin which can be found here:

https://github.com/aslafy-z/helm-git

and installed with:

```
helm plugin install https://github.com/aslafy-z/helm-git --version 0.10.0
```